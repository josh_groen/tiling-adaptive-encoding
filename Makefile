default:
	rm -Rf Video Encode stdout.txt stderr.txt
	g++ -std=c++11 -m64 -Wall -g -O3 -pthread -D__STDC_CONSTANT_MACROS SigSevHandler.cpp NvHWEncoder.cpp NvHWEncoder.h FFmpegWithNvenc.h NvEncoder.h FFmpegWithNvenc.cpp -o Encode $$(pkg-config --libs --cflags libavcodec libavutil libavformat libswscale) -L/usr/lib64 -lnvidia-encode -ldl -L/usr/local/cuda/lib64 -lcuda -lcudart

perf:		
	rm -Rf Video Encode stdout.txt stderr.txt
	g++ -fno-omit-frame-pointer -std=c++11 -m64 -Wall -g -O3 -pthread -D__STDC_CONSTANT_MACROS SigSevHandler.cpp NvHWEncoder.cpp NvHWEncoder.h FFmpegWithNvenc.h NvEncoder.h FFmpegWithNvenc.cpp -o Encode $$(pkg-config --libs --cflags libavcodec libavutil libavformat libswscale) -L/usr/lib64 -lnvidia-encode -ldl -L/usr/local/cuda/lib64 -lcuda -lcudart
test:
	rm -Rf Video Encode stdout.txt stderr.txt
	g++ -std=c++11 -m64 -Wall -g -O3 -pthread -D__STDC_CONSTANT_MACROS SigSevHandler.cpp NvHWEncoder.cpp NvHWEncoder.h FFmpegWithNvenc.h NvEncoder.h FFmpegWithNvenc2.cpp -o Encode $$(pkg-config --libs --cflags libavcodec libavutil libavformat libswscale) -L/usr/lib64 -lnvidia-encode -ldl -L/usr/local/cuda/lib64 -lcuda -lcudart

nosig:
	g++ -std=c++11 -m64 -Wall -g -O3 -pthread -D__STDC_CONSTANT_MACROS NvHWEncoder.cpp NvHWEncoder.h NvEncoder.h FFmpegWithNvenc.cpp FFmpegWithNvenc.h -o Encode $$(pkg-config --libs --cflags libavcodec libavutil libavformat libswscale) -L/usr/lib64 -lnvidia-encode -ldl -L/usr/local/cuda/lib64 -lcuda -lcudart


ffmpeg:
	g++ -std=c++11 -m64 -Wall -g -O3 -pthread StoreAndEncode.cpp -o StoreAndEncode $$(pkg-config --libs --cflags libavcodec libavformat libswscale) -L/usr/lib64 -lnvidia-encode -ldl -L/usr/local/cuda/lib64 -lcuda -lcudart


run:
	rm -f stdout.txt stderr.txt
	./StoreAndEncode 2099_2014-08-12_205956.933908.h264 # >> stdout.txt 2>> stderr.txt
	./StoreAndEncode 2099_2014-08-12_205956.933908.h264 
run2:
	rm -f stdout.txt stderr.txt
	./StoreAndEncode 2099_2014-08-12_205956.933908.0000_0000.h264
debug:
	rm -f stdout.txt stderr.txt
	valgrind --leak-check=full --track-origins=yes ./StoreAndEncode 2099_2014-08-12_205956.933908.h264 >> stdout.txt 2>> stderr.txt
clean:
	rm -Rf Video StoreAndEncode Merging
merge:
	rm -f Merging Test.h264
	g++ -g -O3 Merging.cpp -o Merging $$(pkg-config --libs --cflags libavcodec libavformat libswscale)
