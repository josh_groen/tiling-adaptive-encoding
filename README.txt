The program is not user friendly, and it requires specific version of NVIDIA drivers and keys to compile and panorama video segments to execute. The video streams are
generated from the Bagadus System, thus I'm unsure if it is wise to provide it through Bitbucket.
If you want to fork the program and execute it, you can contact me hoangbn@ifi.uio.no. Necessary information and video files will be provided to compile and a small tutorial 
of how to run the program will also be explained.

SigSevHandler* files were provided by a colleague for debugging and some other files were fetched from the sample program found in NVENC 5.0.1 package from NVIDIA.

The main implementation is FFmpeg* files.