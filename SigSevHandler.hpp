#ifndef SIG_SEG_HANDLER
#define SIG_SEG_HANDLER

#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <stdio.h>
#include <signal.h>
#include <execinfo.h>
#include <ucontext.h>
#include <unistd.h>

namespace SigSevHandler {
    void signalHandler(int sig, siginfo_t *info, void *secret);
    void enableSignalHandler(char *executable);
};

#endif
